# EEG Format Conversion Issue and Solution

The pair of EDF and CSV files have been exported from two different devices. 
<br>See: https://gitlab.com/KrishnaGandhi/eeg-edf-bdf-conversion/-/tree/main/Two%20pairs%20of%20EDF-CSV%20EEG%20data%20exports

ISSUE: 
<br>When the EDF is read out via python it does not match the CSV file output (two channels are incorrect)
<br>See: https://gitlab.com/KrishnaGandhi/eeg-edf-bdf-conversion/-/blob/main/Error_Readout_from_EDF.png 
<br>See: https://gitlab.com/KrishnaGandhi/eeg-edf-bdf-conversion/-/blob/main/CSV_EDF.ipynb

POSSIBLE REASON: 
1. The python script is not reading the EDF file correctly
2. The CSV is not converted to EDF format in the first place

INVESTIGATION:
<br>In testing I found that the conversion for EDF exports from multiple headsets were not reconciling with the the CSV export when the EDF was re-read in python. The problem is that EDF is 16 bit and CSV or higher sampling rate requires 24 bit (perhaps if using the libraries to convert to EDF/EDF+ ?!). 
Since BDF/BDF+ is the 24bit version of EDF/EDF it should then work. The original exported CSV was converted to BDF and read back in python accurately. 
<br>See: https://gitlab.com/KrishnaGandhi/eeg-edf-bdf-conversion/-/blob/main/Correct_Readout_from_BDF.png
<br>See: https://gitlab.com/KrishnaGandhi/eeg-edf-bdf-conversion/-/blob/main/CSV_BDF.ipynb

SOLUTION: 
<br>Use BDF formats for EEG processing. It is requested that all EEG devices provide a BDF/BDF+ export option.
<br>Like this: https://gitlab.com/KrishnaGandhi/eeg-edf-bdf-conversion/-/blob/main/your_file1.bdf


The supporting information was provided by Foad Moradi.
